import datetime
from datetime import datetime, timedelta
from flask import (redirect, url_for, send_file,
                   send_from_directory, render_template,
                   request, jsonify, make_response)
import json
import logging
import os
import requests
import shlex
import subprocess
from server import app
import uuid

languages = ['python', 'javascript', 'perl', 'java', 'php']
logging.basicConfig(format='%(asctime)s %(message)s')

def get_client_path(path): return os.path.join(app.config['client_path'], path)


def get_temp_file(file_content=''):
    """
    Generate a unique filename and writes `file_content` to it.
    :param file_content:
        The file content to write.
    :return:
        The generated file name.
    """
    filename = str(uuid.uuid4())

    with open(filename, 'w') as fileptr:
        fileptr.write(file_content)

    return {'name': filename, 'data': file_content}


def run_command(cmd):
    """
    Run a given shell command.
    :param cmd:
        The command to run.
    :return:
        A tuple of (output, error)
    """
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    return process.communicate()


@app.route('/')
def index():
    """
    Render index page for website.
    """
    team = []
    try:
        resp_str = requests.get("https://api.github.com/repos/coala-analyzer/"
                        "coala/stats/contributors").text
        resp_obj = json.loads(resp_str)

        for member in sorted(resp_obj,
                             key=lambda el: el['total'],
                             reverse=True):
            team.append({'commit_count' : member['total'],
                         'username' : member['author']['login'],
                         'avatar_url' : member['author']['avatar_url']})
    except requests.exceptions.ConnectionError as e:
        logging.error(e)

    return render_template('index.html', team=team)


@app.route('/bear-docs', methods=['GET'])
def beardocs():
    """
    Send the index page for beardocs.
    """
    return send_file(get_client_path('index.html'))


@app.route('/bear-docs/<path:path>', methods=['GET'])
def send_beardocs_static_files(path):
    dirname = os.path.dirname(path)
    file = os.path.basename(path)
    print(dirname)
    static_dir = get_client_path(dirname)
    print(static_dir, file)
    return send_from_directory(static_dir, file)


@app.route('/editor', methods=['GET', 'POST'])
def show_editor():
    return render_template('editor.html', languages=languages)


@app.route('/code/', methods=['POST'])
def run_coala():
    """
    Run coala on the code sent by user and respond with
    the json. This json could be used to display results
    within editor.Default language is python
    :return:
        The output from coala-json.
    """
    user_file = get_temp_file(request.form['code'])
    user_bear = shlex.quote(request.form['bear'])
    print(user_bear)
    try:
        cmd = ("coala-json -I --bears={bears} "
               "--files={files}".format(bears=user_bear,
                                        files=user_file['name']))
        cmd = shlex.split(cmd)
        coala_json, err = run_command(cmd)
    finally:
        os.unlink(user_file['name'])   # delete the file from disk
    return coala_json


@app.route('/beardoc', methods=['POST'])
def get_bear_description():
    language = request.form['userLang']
    userName = request.form['userName']
    print(language, userName)
    if userName == "Developers":
        language = shlex.quote(language)
        cmd = ("coala -p {language}".format(language=language))
        cmd = shlex.split(cmd)
        bears_desc, err = run_command(cmd)
        bears_desc = str(bears_desc).split("\\n")[-3:-1]
        print(bears_desc)

        bear_desc_dict = {'can_detect': bears_desc[0].split(':')[-1],
                          'can_fix': bears_desc[1].split(':')[-1]}
        return jsonify(**bear_desc_dict)
    else:
        NotImplementedYet = {}
        return jsonify(**NotImplementedYet)


@app.route('/bear/<language>', methods=['GET'])
def filter_bear_by_language(language='python'):
    """
    Filter bears by a given programming language.
    :return:
        A list of available bears for the given language.
    """
    language = shlex.quote(language)
    cmd = ("coala-json -B -l {language}".format(language=language))
    cmd = shlex.split(cmd)
    bears, err = run_command(cmd)
    return bears


@app.route('/robots.txt/')
def robots():
    return("User-agent: *\nDisallow: /editor/\nDisallow: /code/")


@app.route('/sitemap.xml', methods=['GET'])
def sitemap():
    try:
        """Generate sitemap.xml. Makes a list of urls and date
           modified.
        """
        pages = []
        ten_days_ago = (datetime.now() - timedelta(days=7)).date().isoformat()
        # static pages
        for rule in app.url_map.iter_rules():
            if "GET" in rule.methods and len(rule.arguments) == 0:
                pages.append(["http://coala-analyzer.org" +
                              str(rule.rule), ten_days_ago])

        sitemap_xml = render_template('sitemap_template.xml', pages=pages)
        response = make_response(sitemap_xml)
        response.headers["Content-Type"] = "application/xml"

        return response
    except Exception as e:
        return(str(e))


@app.errorhandler(404)
def page_not_found(error):
    return render_template('page_not_found.html'), 404
