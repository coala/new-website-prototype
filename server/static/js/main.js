var dom = function(id) {
    return document.getElementById(id);
}
// Set User for landing page. Python + Developers
var userLang = "python", userType = 0;
// preMemoized results
var oldUserLang = "", oldUserType=0;
var setUser = function(type) {
  userType = +type;
  if(+type){
    dom('devOptions').style.display = "none";
  } else {
    dom('devOptions').style.display = "block";
  }
}
// Avoid ajax for unchanged form inputs. Memoize the last userDetails.
var isSameCall = function(newUserLang, newUserType){
  return true ? (newUserLang===oldUserLang) && (newUserType===oldUserType): false;
}
var setUserLang = function(lang) {
  userLang = lang;
}
var showBearDoc = function() {
  var userName = +userType ? "Researchers" : "Developers";
  userLang = +userType ? "NA" : userLang;
  console.log("isSameCall: " + isSameCall(userLang, userType));

  $(".bearLang").hide();

  if(!isSameCall(userLang, userType)) {
    $.ajax({url: "/beardoc",
            data: {"userLang":userLang,"userName":userName},
            dataType:"json",
            method:'POST',
            success: function(data) {
                if(!Object.keys(data).length) {
                  $("#not_found").html("<h2 style='color:white;'>Not Implemented yet</h2>");
                  $("#not_found").show();
                  $("#can_detect_head").hide();
                  $("#can_fix_head").hide();
                } else {
                  $("#not_found").hide();
                  if(data.hasOwnProperty('can_detect')) {
                      $("#can_detect_head").show();
                      $("#can_detect").html(data.can_detect);
                  }
                  if(data.hasOwnProperty('can_fix')) {
                      $("#can_fix_head").show();
                      $("#can_fix").html(data.can_fix);
                  }
                }
                $("#coalaUse").carousel(1);
                $(".left").show();
            },
            error: function(jqXHR, err) {
                console.log("Error",err, jqXHR.status);
                if(jqXHR.status !== 200)
                  console.log("Error",err, jqXHR.status);
            }
    });
  } else {
    $("#coalaUse").carousel(1);
    $(".left").show();
  }
  oldUserLang = userLang; oldUserType = userType;
}
// Set height for blocks
var blockIds = ['home', 'why', 'donate', 'team'];
blockIds.forEach(function(id) {
    dom(id).style.minHeight = window.innerHeight + 'px';
    dom(id).style.minWidth = 0.9*window.innerWidth + 'px';
});
// Type language list
var LANGUAGE_LIST = ['C++',
                     'Go',
                     'Java',
                     'JavaScript',
                     'Python',
                     'PHP',
                     'Perl']
$(function(){
    $("#coala-language").typed({
        strings: LANGUAGE_LIST,
        contentType: 'text',
        shuffle: true,
        startDelay: 500,
        backDelay: 500,
        loop: true,
        loopCount: false,
        showCursor: true,
        cursorChar: "|"
    });
});
// Scroll reveal
window.sr = ScrollReveal();
