from flask import Flask
import os

current_path = os.path.dirname(__file__)

app = Flask(__name__, static_url_path='')
app.secret_key = 'add_later'
app.config['client_path'] = os.path.abspath(
    os.path.join(current_path, '..', 'bear-docs'))

import server.routes
