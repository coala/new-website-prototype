### coala website

### Installation for development

###### Fetch the source code
```sh
  git clone https://gitlab.com/coala/website.git
```

Enter the `website` directory and,

###### Install the dependencies
```
  bower install
  pip install -r requirements.txt
```

Once the dependencies have been installed, launch the application
by
```sh
python3 runserver.py
```


