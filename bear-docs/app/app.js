'use strict';

angular
  .module('coalabearDocsApp', ['ngRoute','angular.filter'])
  .run(['$rootScope', '$http', function($rootScope, $http) {
    $http.get("/bear-docs/data/bears.json")
      .then(function(coala_bears) {
    console.log(coala_bears);
        $rootScope.bears = coala_bears.data.bears;
    });
  }])
  .config(['$routeProvider',function ($routeProvider) {
    $routeProvider
      .when('/bear-docs', {
        templateUrl: '/bear-docs/app/views/bears.html',
        controller: 'BearsCtrl',
        controllerAs: 'bears'
      })
      .when('/bear-docs/:bearName', {
        templateUrl: '/bear-docs/app/views/detail.html',
        controller: 'DetailCtrl',
        controllerAs: 'details'
      })
      .when('/bear-docs/lang/:language', {
        templateUrl: '/bear-docs/app/views/language.html',
        controller: 'LangCtrl',
        controllerAs: 'language'
      })
      .otherwise({
        redirectTo: '/bear-docs'
      });
  }]);
